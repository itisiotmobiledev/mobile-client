package com.itis.tamagotchi.application;

import android.app.Application;
import android.content.Intent;
import android.support.v4.content.ContextCompat;

import com.google.firebase.FirebaseApp;
import com.itis.tamagotchi.models.MqttClient.MqttManager;
import com.itis.tamagotchi.services.BackgroundMqttService;
import com.itis.tamagotchi.services.FirebasePushService;
import com.itis.tamagotchi.services.MqttManagerService;

import org.eclipse.paho.android.service.MqttService;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.getInstance();

        startService(new Intent(this, FirebasePushService.class));

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
