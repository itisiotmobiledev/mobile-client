package com.itis.tamagotchi.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.itis.tamagotchi.R;
import com.itis.tamagotchi.adapters.ProgressAdapter;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.ReadMessageBroadcast;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.SendMessageBroadcast;
import com.itis.tamagotchi.models.MqttClient.MqttManager;
import com.itis.tamagotchi.models.StateSaver.SaverStatus;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiCommands;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiResponseHandler;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiStatus;

public class StatisticsFragment extends RootFragment {

    private ListView listProgresses;
    private BroadcastReceiver receiverProgresses;
    private TamagotchiResponseHandler responseHandler;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View fragmentView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(fragmentView, savedInstanceState);
        listProgresses = fragmentView.findViewById(R.id.list_progresses);
        responseHandler = new TamagotchiResponseHandler(this);
        receiverProgresses = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String topic = intent.getStringExtra(ReadMessageBroadcast.TOPIC_EXTRA_KEY);
                String message = intent.getStringExtra(ReadMessageBroadcast.MESSAGE_EXTRA_KEY);
                responseHandler.handleStatistics(topic, message);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiverProgresses, new IntentFilter(
                ReadMessageBroadcast.ACTION_READ_MESSAGE
        ));

        Intent intent = new Intent();
        intent.setAction(SendMessageBroadcast.ACTION);
        getActivity().sendBroadcast(SendMessageBroadcast.getIntentForMe(
                MqttManager.TAMAGOTCHI_MAIN_TOPIC,
                TamagotchiCommands.GET_STATUS_COMMAND
        ));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiverProgresses);
    }

    @Override
    public void showProgresses(TamagotchiStatus[] statuses) {
        super.showProgresses(statuses);
        ProgressAdapter adapter = new ProgressAdapter(getContext(),
                R.layout.action_progress_view, statuses);
        listProgresses.setAdapter(adapter);
    }
}
