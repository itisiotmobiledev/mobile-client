package com.itis.tamagotchi.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.itis.tamagotchi.R;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.ReadMessageBroadcast;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.SendMessageBroadcast;
import com.itis.tamagotchi.models.MqttClient.MqttManager;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiCommands;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiResponseHandler;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Region;

import java.util.Iterator;

public class TamagotchiFragment extends RootFragment implements BeaconConsumer {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tamagotchi_fragment, container, false);
    }


    private TamagotchiResponseHandler responseHandler;

    private ConstraintLayout rootLayout;
    private FloatingActionsMenu fab;
    private ImageView statusView;

    private BroadcastReceiver readMessageReceiver;
    private BroadcastReceiver readErrorReceiver;

    private MediaPlayer eatButtonClickSound;
    private MediaPlayer sleepButtonClickSound;
    private MediaPlayer toiletButtonClickSound;
    private MediaPlayer isSleepSound;

    private BeaconManager beaconManager;
    private static final String BEACON_CHANNEL = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";

    @Override
    public void onViewCreated(@NonNull View viewFragment, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(viewFragment, savedInstanceState);

        rootLayout = viewFragment.findViewById(R.id.root_layout);
        fab = viewFragment.findViewById(R.id.fab);
        statusView = viewFragment.findViewById(R.id.status_view);

        FloatingActionButton eatingButton = viewFragment.findViewById(R.id.button_eating);
        eatingButton.setOnClickListener(eatingButtonClick);

        FloatingActionButton sleepButton  = viewFragment.findViewById(R.id.button_sleep);
        sleepButton.setOnClickListener(sleepButtonClick);

        FloatingActionButton toiletButton = viewFragment.findViewById(R.id.button_toilet);
        toiletButton.setOnClickListener(toiletButtonClick);

        responseHandler = new TamagotchiResponseHandler(this);

        readMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String topic = intent.getStringExtra(ReadMessageBroadcast.TOPIC_EXTRA_KEY);
                String message = intent.getStringExtra(ReadMessageBroadcast.MESSAGE_EXTRA_KEY);

                responseHandler.handle(topic, message);
            }
        };
        readErrorReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String error = intent.getStringExtra(ReadMessageBroadcast.ERROR_EXTRA_KEY);
                showMessage(error);
            }
        };

        eatButtonClickSound    = MediaPlayer.create(getContext(), R.raw.eating_sound);
        sleepButtonClickSound  = MediaPlayer.create(getContext(), R.raw.sleep_sound);
        toiletButtonClickSound = MediaPlayer.create(getContext(), R.raw.toilet_sound);
        isSleepSound           = MediaPlayer.create(getContext(), R.raw.is_sleep_sound);

        beaconManager = BeaconManager.getInstanceForApplication(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(readMessageReceiver,
                new IntentFilter(ReadMessageBroadcast.ACTION_READ_MESSAGE));
        getActivity().registerReceiver(readErrorReceiver,
                new IntentFilter(ReadMessageBroadcast.ACTION_ERROR));
        scan();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(readErrorReceiver);
        getActivity().unregisterReceiver(readMessageReceiver);
        beaconManager.removeAllMonitorNotifiers();
    }

    @Override
    public void setStatusTamagotchi(int status) {
        statusView.setImageDrawable(ContextCompat.getDrawable(getContext(), status));
    }

    public void showMessage(String message) {
        if(getView() == null) return;
        Snackbar.make(getView().getRootView(), message, Snackbar.LENGTH_LONG)
                .show();
    }

    private void showProgress() {

    }

    private void hideProgress() {

    }

    private boolean isBeacon = false;

    private View.OnClickListener eatingButtonClick = v -> {
        if(isBeacon) {
            showMessage(getString(R.string.go_to_me));
            return;
        }

        if(isSleep) {
            isSleepSound.start();
            return;
        }

        eatButtonClickSound.start();

        String topic = MqttManager.TAMAGOTCHI_MAIN_TOPIC;
        String commandFeed = TamagotchiCommands.FEED_COMMAND;

        getActivity().sendBroadcast(SendMessageBroadcast.getIntentForMe(topic, commandFeed));

        fab.collapse();
    };

    private View.OnClickListener sleepButtonClick = v -> {
        if(isBeacon) {
            showMessage(getString(R.string.go_to_me));
            return;
        }

        if(isSleep) {
            isSleepSound.start();
            return;
        }

        sleepButtonClickSound.start();

        String topic = MqttManager.TAMAGOTCHI_MAIN_TOPIC;
        String commandToSleep = TamagotchiCommands.SLEEP_COMMAND(10);

        getActivity().sendBroadcast(SendMessageBroadcast.getIntentForMe(topic, commandToSleep));

        fab.collapse();
    };

    private View.OnClickListener toiletButtonClick = v -> {
        if(isBeacon) {
            showMessage(getString(R.string.go_to_me));
            return;
        }

        if(isSleep) {
            isSleepSound.start();
            return;
        }

        toiletButtonClickSound.start();

        String topic = MqttManager.TAMAGOTCHI_MAIN_TOPIC;
        String commandToSleep = TamagotchiCommands.TOILET_COMMAND;

        getActivity().sendBroadcast(SendMessageBroadcast.getIntentForMe(topic, commandToSleep));

        fab.collapse();
    };

    private static final int REQUEST_PERMISSION_CODE = 10001;
    private void scan() {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH) ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            beaconManager
                    .getBeaconParsers()
                    .add(new BeaconParser()
                            .setBeaconLayout(BEACON_CHANNEL));
            beaconManager.bind(this);
        }
        else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[] { Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_CODE)
            scan();
    }

    @Override
    public void onBeaconServiceConnect() {
        beaconManager.removeAllMonitorNotifiers();
        beaconManager.addRangeNotifier((collection, region) -> {
            if(collection.size() > 0) {
                Beacon[] beacons = new Beacon[collection.size()];
                collection.toArray(beacons);

                for (Beacon beacon:
                     beacons) {
                    if(beacon.getId2().toInt() == 5) {
                        isBeacon = true;
                        return;
                    }
                }
            }
            isBeacon = false;
        });
        try {
            beaconManager.startRangingBeaconsInRegion(new Region("", null, null, null));
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Context getApplicationContext() {
        return null;
    }

    @Override
    public void unbindService(ServiceConnection serviceConnection) {

    }

    @Override
    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        return false;
    }
}
