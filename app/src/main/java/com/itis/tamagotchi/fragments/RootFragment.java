package com.itis.tamagotchi.fragments;

import android.support.v4.app.Fragment;

import com.itis.tamagotchi.models.tamagotchi.TamagotchiStatus;

public class RootFragment extends Fragment {

    public void setStatusTamagotchi(int status) {}

    public void showProgresses(TamagotchiStatus[] statuses) {}

    protected boolean isSleep;
    public void setStatusSleep(boolean statusSleep) {
        isSleep = statusSleep;
    }
}
