package com.itis.tamagotchi.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.itis.tamagotchi.R;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiStatus;

public class ProgressAdapter extends ArrayAdapter {

    private LayoutInflater inflater;
    private int layout;
    private TamagotchiStatus[] statuses;

    public ProgressAdapter(@NonNull Context context, int resource,
                           @NonNull TamagotchiStatus[] statuses) {
        super(context, resource, statuses);
        inflater = LayoutInflater.from(context);
        layout = resource;
        this.statuses = statuses;
    }

    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent) {
        View itemView = inflater.inflate(layout, parent, false);

        if(statuses[position] == null) return itemView;

        TextView labelView = itemView.findViewById(R.id.label_view);
        ProgressBar progressBar = itemView.findViewById(R.id.progress_view);

        labelView.setText(statuses[position].getTitle());
        progressBar.setProgress(statuses[position].getCurrent());
        progressBar.setMax(statuses[position].getMax());

        return itemView;
    }

    @Override
    public int getCount() {
        return statuses.length;
    }
}
