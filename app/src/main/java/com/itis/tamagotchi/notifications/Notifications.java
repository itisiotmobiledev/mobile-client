package com.itis.tamagotchi.notifications;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

public final class Notifications {
    public static final String MQTT_MANAGER_CHANNEL_ID_BKG_NOTIFICATION_SERVICE = "mqtt_manager_background_notification_channel";
    public static final String MQTT_MANAGER_CHANNEL_NAME_BKG_NOTIFICATION_SERVICE = "mqtt_manager_background_notification_channel";

    public static final String MQTT_CHANNEL_ID_BKG_NOTIFICATION_SERVICE = "mqtt_background_notification_channel";
    public static final String MQTT_CHANNEL_NAME_BKG_NOTIFICATION_SERVICE = "mqtt_background_notification_channel";

    public static final String FIREBASE_CHANNEL_ID_BAG_NOTIFICATION_SERVICE = "firebase_background_notification_channel";
    public static final String FIREBASE_CHANNEL_NAME_BAG_NOTIFICATION_SERVICE = "firebase_background_notification_channel";

    public static final String HUNGRY_NOTIFICATION_CHANNEL_ID = "hungry_channel_id";
    public static final String HUNGRY_NOTIFICATION_CHANNEL    = "hungry_channel_name";

    public static final String SLEEP_NOTIFICATION_CHANNEL_ID = "sleep_channel_id";
    public static final String SLEEP_NOTIFICATION_CHANNEL    = "sleep_channel_name";

    public static final String TOILET_NOTIFICATION_CHANNEL_ID = "toilet_channel_id";
    public static final String TOILET_NOTIFICATION_CHANNEL    = "toilet_channel_name";


    public static final String NOTIFICATION_DATA_KEY              = "type";
    public static final int NOTIFICATION_ID                       = 10011;
    public static final String NOTIFICATION_DATA_ID_KEY           = "notification_id";
    public static final String NOTIFICATION_DATA_VALUE_HUNGRY     = "hungry";
    public static final String NOTIFICATION_DATA_VALUE_SLEEP      = "sleep";
    public static final String NOTIFICATION_DATA_VALUE_TOILET     = "toilet";
    public static final String NOTIFICATION_DATA_VALUE_WALKING_UP = "walking_up";

    @TargetApi(26)
    public static void createChannel(Context context, String channelId, String channelName) {
        NotificationChannel channel = new NotificationChannel(channelId,
                channelName,
                NotificationManager.IMPORTANCE_NONE);

        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
                .createNotificationChannel(channel);
    }


}
