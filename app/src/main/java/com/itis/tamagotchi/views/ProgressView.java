package com.itis.tamagotchi.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.itis.tamagotchi.R;

import javax.annotation.Nullable;

public class ProgressView extends LinearLayout {

    private TextView labelView;
    private ProgressBar progressView;
    private Button buttonView;

    public ProgressView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        labelView = getRootView().findViewById(R.id.label_view);
    }

    public ProgressView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgressView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        labelView = getRootView().findViewById(R.id.label_view);
    }

    private boolean viewsIsNull() {
        return labelView == null || progressView == null || buttonView == null;
    }

    private void initialViews() {
        FrameLayout childLayout = (FrameLayout) getChildAt(0);
        labelView = (TextView) childLayout.getChildAt(0);
        progressView = (ProgressBar) childLayout.getChildAt(1);
        buttonView = (Button) getChildAt(1);
    }

    public void setTextLabel(String textLabel) {
        if(viewsIsNull()) {
            initialViews();
        }
        labelView.setText(textLabel);
    }

    public void setProgress(int progress) {
        if(viewsIsNull()) {
            initialViews();
        }
        progressView.setProgress(progress);
    }

    public void setNameAction(String actionName) {
        if(viewsIsNull()) {
            initialViews();
        }
        buttonView.setText(actionName);
    }

    public  void setButtonClickListener(OnClickListener clickListener) {
        buttonView.setOnClickListener(clickListener);
    }
}
