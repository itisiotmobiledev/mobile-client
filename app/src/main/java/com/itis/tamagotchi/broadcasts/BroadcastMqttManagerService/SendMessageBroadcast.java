package com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.itis.tamagotchi.models.MqttClient.MqttManager;

import org.eclipse.paho.client.mqttv3.MqttException;

public class SendMessageBroadcast extends BroadcastReceiver {

    public static final String ACTION = "com.itis.tamagotchi.send.message";

    public static final String MESSAGE_EXTRA_KEY = "message";
    public static final String TOPIC_EXTRA_KEY   = "key";

    private MqttManager mqttManager;

    public SendMessageBroadcast() {}

    public SendMessageBroadcast(MqttManager mqttManager) {
        this.mqttManager = mqttManager;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String topic   = intent.getStringExtra(MESSAGE_EXTRA_KEY);
        String message = intent.getStringExtra(TOPIC_EXTRA_KEY);

        if(TextUtils.isEmpty(topic) || TextUtils.isEmpty(message))
            return;

        try {
            mqttManager.sendMessage(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();

        }
    }

    public static Intent getIntentForMe(String topic, String message) {
        Intent intent = new Intent();
        intent.setAction(SendMessageBroadcast.ACTION);
        intent.putExtra(SendMessageBroadcast.TOPIC_EXTRA_KEY, topic);
        intent.putExtra(SendMessageBroadcast.MESSAGE_EXTRA_KEY, message);

        return intent;
    }
}
