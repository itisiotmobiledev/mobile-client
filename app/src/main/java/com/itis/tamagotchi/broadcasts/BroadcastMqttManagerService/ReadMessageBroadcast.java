package com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService;

import android.content.Intent;

public class ReadMessageBroadcast {

    public static final String ACTION_READ_MESSAGE = "com.itis.tamagotchi.read.message";
    public static final String ACTION_ERROR        = "com.itis.tamagotchi.read.error";

    public static final String MESSAGE_EXTRA_KEY = "message";
    public static final String TOPIC_EXTRA_KEY   = "topic";
    public static final String ERROR_EXTRA_KEY   = "error";


    public static Intent getIntentForMe(String topic, String message) {
        Intent intent = new Intent();
        intent.setAction(ACTION_READ_MESSAGE);
        intent.putExtra(ReadMessageBroadcast.TOPIC_EXTRA_KEY, topic);
        intent.putExtra(ReadMessageBroadcast.MESSAGE_EXTRA_KEY, message);

        return intent;
    }
}
