package com.itis.tamagotchi.activities;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.itis.tamagotchi.R;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.SendMessageBroadcast;
import com.itis.tamagotchi.fragments.StatisticsFragment;
import com.itis.tamagotchi.fragments.TamagotchiFragment;
import com.itis.tamagotchi.models.MqttClient.MqttManager;
import com.itis.tamagotchi.models.Router;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiCommands;
import com.itis.tamagotchi.notifications.Notifications;
import com.itis.tamagotchi.services.BackgroundMqttService;
import com.itis.tamagotchi.services.MqttManagerService;

public class MainActivity extends AppCompatActivity {

    private Router router;

    private BottomNavigationView navigation;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.nav_tamagochi:
                        router.showFragment(new TamagotchiFragment());
                        break;

                    case R.id.nav_statistics:
                        router.showFragment(new StatisticsFragment());
                        break;
                }

                item.setChecked(true);
                return false;
            };

    private Intent intentMqttBackgroundService;
    private Intent intentMqttManagerService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handleNotification();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.app_name));

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.getMenu().findItem(R.id.nav_tamagochi).setChecked(true);

        router = new Router(this);
        router.startFragment(new TamagotchiFragment());

        intentMqttBackgroundService = new Intent(getApplicationContext(), BackgroundMqttService.class);
        intentMqttManagerService = new Intent(getApplicationContext(), MqttManagerService.class);

        ContextCompat.startForegroundService(getApplicationContext(), intentMqttBackgroundService);
        ContextCompat.startForegroundService(getApplicationContext(), intentMqttManagerService);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(intentMqttBackgroundService);
        stopService(intentMqttManagerService);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        stopService(intentMqttBackgroundService);
        stopService(intentMqttManagerService);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void handleNotification() {
        Intent intent = getIntent();
        int notificationId = intent.getIntExtra(Notifications.NOTIFICATION_DATA_ID_KEY, 0);
        if(notificationId == 0) return;

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);

        String type = intent.getStringExtra(Notifications.NOTIFICATION_DATA_KEY);

        String topic = MqttManager.TAMAGOTCHI_MAIN_TOPIC;
        String command = null;

        if(type != null && !type.equals(Notifications.NOTIFICATION_DATA_VALUE_WALKING_UP)) {
            switch (type) {
                case Notifications.NOTIFICATION_DATA_VALUE_HUNGRY:
                    command = TamagotchiCommands.FEED_COMMAND;
                    break;

                case Notifications.NOTIFICATION_DATA_VALUE_SLEEP:
                    command = TamagotchiCommands.SLEEP_COMMAND(10);
                    break;

                case Notifications.NOTIFICATION_DATA_VALUE_TOILET:
                    command = TamagotchiCommands.TOILET_COMMAND;
                    break;

                    default: return;
            }

            if(TextUtils.isEmpty(command)) return;
            sendBroadcast(SendMessageBroadcast.getIntentForMe(topic, command));
        }
    }
}
