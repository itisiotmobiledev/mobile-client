package com.itis.tamagotchi.services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.support.v4.app.NotificationCompat;

import com.itis.tamagotchi.R;
import com.itis.tamagotchi.notifications.Notifications;

import org.eclipse.paho.android.service.MqttService;

public class BackgroundMqttService extends MqttService {
    private static final int NOTIFICATION_ID = 10002;


    @Override
    public void onCreate() {
        super.onCreate();
        showNotification();
    }

    @TargetApi(26)
    private void showNotification() {

        Notifications.createChannel(getApplicationContext(),
                Notifications.MQTT_CHANNEL_ID_BKG_NOTIFICATION_SERVICE,
                Notifications.MQTT_CHANNEL_NAME_BKG_NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(this,
                Notifications.MQTT_CHANNEL_ID_BKG_NOTIFICATION_SERVICE)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.foreground_service_notification_title))
                .setContentText(getString(R.string.foreground_mqtt_service_notification_content)).build();

        startForeground(NOTIFICATION_ID, notification);
    }
}
