package com.itis.tamagotchi.services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.itis.tamagotchi.R;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.ReadMessageBroadcast;
import com.itis.tamagotchi.broadcasts.BroadcastMqttManagerService.SendMessageBroadcast;
import com.itis.tamagotchi.models.MqttClient.MqttManager;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiCommands;
import com.itis.tamagotchi.notifications.Notifications;

import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import javax.annotation.Nullable;

public class MqttManagerService extends Service {
    private static final int NOTIFICATION_ID = 10001;

    private BeaconManager beaconManager;

    private MqttManager mqttManager;
    private BroadcastReceiver sendMessageBroadcast;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showNotification();

        mqttManager = new MqttManager(getApplication());
        sendMessageBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String topic = intent.getStringExtra(SendMessageBroadcast.TOPIC_EXTRA_KEY);
                String message = intent.getStringExtra(SendMessageBroadcast.MESSAGE_EXTRA_KEY);

                try {
                    if(mqttManager.isConnected()) {
                        mqttManager.sendMessage(topic, message);
                    }
                } catch (MqttException exception) {
                    exception.printStackTrace();
                }
            }
        };
        getApplication().registerReceiver(sendMessageBroadcast,
                new IntentFilter(SendMessageBroadcast.ACTION));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            mqttManager.connectToBroker(connectListener);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.e(getClass().getName(), e.getMessage());
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mqttManager.disconnect();
        getApplication().unregisterReceiver(sendMessageBroadcast);
    }

    @TargetApi(26)
    private void showNotification() {

        Notifications.createChannel(getApplicationContext(),
                Notifications.MQTT_MANAGER_CHANNEL_ID_BKG_NOTIFICATION_SERVICE,
                Notifications.MQTT_MANAGER_CHANNEL_NAME_BKG_NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(this,
                Notifications.MQTT_MANAGER_CHANNEL_ID_BKG_NOTIFICATION_SERVICE)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(getString(R.string.foreground_service_notification_title))
                .setContentText(getString(R.string.foreground_mqtt_service_notification_content)).build();

        startForeground(NOTIFICATION_ID, notification);
    }

    private void error(String error) {
        Intent intent = new Intent();
        intent.setAction(ReadMessageBroadcast.ACTION_ERROR);
        intent.putExtra(ReadMessageBroadcast.ERROR_EXTRA_KEY, error);
        sendBroadcast(intent);
    }

    private IMqttActionListener connectListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            try {
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_MAIN_TOPIC,
                        tamagotchiMainTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_PET_STATE_TOPIC,
                        tamagotchiPetStateTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_HUNGRY_STATE_TOPIC,
                        tamagotchiHungryStateTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_SLEEP_STATUS_TOPIC,
                        tamagotchiSleepStateTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_TOILET_STATUS_TOPIC,
                        tamagotchiToiletTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_WANNA_PLAY_STATUS_TOPIC,
                        tamagotchiWannaPlayStateTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_WANNA_WALK_STATUS_TOPIC,
                        tamagotchiWannaWalkSateTopicSubscribeListener);
                mqttManager.subscribeToTopic(MqttManager.TAMAGOTCHI_STATUS_CHECK_SLEEP_TOPIC,
                        tamagotchiPetStateTopicSubscribeListener);
                mqttManager.setCallback(mqttCallback);
            } catch (MqttException e) {
                e.printStackTrace();
                Log.e(getClass().getName(), e.getMessage());
            }
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            error(getString(R.string.error_on_subscribing));
        }
    };

    private MqttCallback mqttCallback = new MqttCallback() {
        @Override
        public void connectionLost(Throwable cause) {

        }

        @Override
        public void messageArrived(String topic, MqttMessage message) {
            sendBroadcast(ReadMessageBroadcast.getIntentForMe(topic, message.toString()));
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {

        }
    };

    private IMqttActionListener tamagotchiMainTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            String topic = MqttManager.TAMAGOTCHI_MAIN_TOPIC;
            String message = TamagotchiCommands.GET_STATUS_COMMAND;

            try {
                mqttManager.sendMessage(topic, message);
                mqttManager.sendMessage(topic, TamagotchiCommands.IS_SLEEP_COMMAND);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
            error(getString(R.string.error_on_subscribing_in_main_topic));
        }
    };

    private IMqttActionListener tamagotchiPetStateTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            String topic = MqttManager.TAMAGOTCHI_MAIN_TOPIC;
            String message = TamagotchiCommands.GET_STATUS_COMMAND;

            try {
                mqttManager.sendMessage(topic, message);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
        }
    };

    private IMqttActionListener tamagotchiHungryStateTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
            int a = 1;
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
            error(getString(R.string.error_on_subscribing_in_hungry_state_topic));
        }
    };

    private IMqttActionListener tamagotchiSleepStateTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
            error(getString(R.string.error_on_subscribing_in_sleep_state_topic));

        }
    };

    private IMqttActionListener tamagotchiToiletTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
        }
    };

    private IMqttActionListener tamagotchiWannaPlayStateTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
            error(getString(R.string.error_on_subscribing_in_wanna_play_state_topic));
        }
    };

    private IMqttActionListener tamagotchiWannaWalkSateTopicSubscribeListener = new IMqttActionListener() {
        @Override
        public void onSuccess(IMqttToken asyncActionToken) {
        }

        @Override
        public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
            exception.printStackTrace();
            error(getString(R.string.error_on_subscribing_in_wanna_walk_state_topic));
        }
    };
}