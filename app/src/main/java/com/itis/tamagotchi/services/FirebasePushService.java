package com.itis.tamagotchi.services;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.itis.tamagotchi.R;
import com.itis.tamagotchi.activities.MainActivity;
import com.itis.tamagotchi.models.database.DatabaseInfo;
import com.itis.tamagotchi.models.database.Token;
import com.itis.tamagotchi.notifications.Notifications;

import java.util.Map;

public class FirebasePushService extends FirebaseMessagingService {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> data = remoteMessage.getData();
        if(data.size() > 0) {
            String type = data.get(Notifications.NOTIFICATION_DATA_KEY);
            if(type != null) {
                if(type.equals(Notifications.NOTIFICATION_DATA_VALUE_WALKING_UP)) {
                    createDefaultNotification(type);
                }else {
                    createActionNotification(type);
                }
            }
        }
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onNewToken(String pushToken) {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        database.collection(DatabaseInfo.DEVICE_COLLECTION)
                .document(DatabaseInfo.DEVICE_DOCUMENT)
                .set(new Token(pushToken));
    }

    @TargetApi(26)
    private void createChannel(NotificationManager notificationManager, String type) {
        NotificationChannel channel1 = new NotificationChannel(type, type,
                NotificationManager.IMPORTANCE_DEFAULT);

        Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"+ getApplicationContext().getPackageName() + "/" + R.raw.baby_sound);

        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();

        channel1.setSound(soundUri, audioAttributes);

        notificationManager.createNotificationChannel(channel1);
    }


    private void createActionNotification(String type) {
        int iconId = 0;
        String textButton = null;

        switch (type) {
            case  Notifications.NOTIFICATION_DATA_VALUE_HUNGRY:
                iconId = R.drawable.ic_cup;
                textButton = getString(R.string.seating);
                break;

            case  Notifications.NOTIFICATION_DATA_VALUE_SLEEP:
                iconId = R.drawable.ic_sleep;
                textButton = getString(R.string.sleeping);
                break;

            case  Notifications.NOTIFICATION_DATA_VALUE_TOILET:
                iconId = R.drawable.ic_toilet;
                textButton = getString(R.string.go_to_toilet);
                break;

            case Notifications.NOTIFICATION_DATA_VALUE_WALKING_UP:
                break;

                default:return;
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Notifications.NOTIFICATION_DATA_KEY, type);
        intent.putExtra(Notifications.NOTIFICATION_DATA_ID_KEY, type.hashCode());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent actionPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Action action = new NotificationCompat
                .Action
                .Builder(iconId, textButton, actionPendingIntent)
                .build();

        NotificationManager mNotificationManager =
                (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);

        createChannel(mNotificationManager, type);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), type)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.i_needed))
                        .setAutoCancel(true)
                        .addAction(action);

        mNotificationManager.notify(type.hashCode(), mBuilder.build());
    }

    private void createDefaultNotification(String type) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Notifications.NOTIFICATION_DATA_KEY, type);
        intent.putExtra(Notifications.NOTIFICATION_DATA_ID_KEY, Notifications.NOTIFICATION_ID);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent actionPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager mNotificationManager =
                (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);

        createChannel(mNotificationManager, type);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), type)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.i_walking))
                        .setAutoCancel(true)
                        .setContentIntent(actionPendingIntent);

        mNotificationManager.notify(Notifications.NOTIFICATION_ID, mBuilder.build());
    }
}
