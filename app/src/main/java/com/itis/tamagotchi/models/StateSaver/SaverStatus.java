package com.itis.tamagotchi.models.StateSaver;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.itis.tamagotchi.models.tamagotchi.TamagotchiStatus;

public class SaverStatus {

    private static final String PREFERENCES_NAME = "PET_STATUS";

    private SharedPreferences statusPreferences;
    private Gson gson;

    public SaverStatus(Context context) {
        statusPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    public void saveState(TamagotchiStatus status) {
        if(status == null) return;

        String key = status.getTitle();
        String jsonData = gson.toJson(status);

        statusPreferences.edit()
                .putString(key, jsonData)
                .apply();
    }

    public TamagotchiStatus readStatus(String key) {
        String jsonData = statusPreferences.getString(key, null);

        if(TextUtils.isEmpty(jsonData)) return null;

        return gson.fromJson(jsonData, TamagotchiStatus.class);
    }
}
