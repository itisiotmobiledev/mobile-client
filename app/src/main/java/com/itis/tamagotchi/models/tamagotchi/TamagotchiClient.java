package com.itis.tamagotchi.models.tamagotchi;

import com.itis.tamagotchi.models.MqttClient.MqttManager;

import org.eclipse.paho.client.mqttv3.MqttException;

public class TamagotchiClient {
    private MqttManager mqttManager;

    public TamagotchiClient(MqttManager mqttManager) {
        this.mqttManager = mqttManager;
    }


    public void eat() throws MqttException {
        if(mqttManager.isConnected()) {
            String command = TamagotchiCommands.FEED_COMMAND;
            String topic   = MqttManager.TAMAGOTCHI_MAIN_TOPIC;

            mqttManager.sendMessage(topic, command);
        }
    }

    public void sleep() {
        if(mqttManager.isConnected()) {

        }
    }
}
