package com.itis.tamagotchi.models.tamagotchi;

public final class TamagotchiStatuses {
    public static final String HUNGRY_STATUS     = "hungry_status";
    public static final String SLEEP_STATUS      = "sleep_status";
    public static final String TOILET_STATUS     = "toilet_status";
    public static final String WANNA_WALK_STATUS = "wanna_walk_status";
    public static final String WANNA_PLAY_STATUS = "wanna_play_status";
}
