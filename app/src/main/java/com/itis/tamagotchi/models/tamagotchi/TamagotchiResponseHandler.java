package com.itis.tamagotchi.models.tamagotchi;

import com.google.gson.Gson;
import com.itis.tamagotchi.R;
import com.itis.tamagotchi.fragments.RootFragment;
import com.itis.tamagotchi.models.MqttClient.MqttManager;
import com.itis.tamagotchi.models.StateSaver.SaverStatus;

public class TamagotchiResponseHandler {
    private RootFragment fragment;
    private SaverStatus saverStatus;
    private Gson gson;

    public TamagotchiResponseHandler(RootFragment fragment) {
        this.fragment = fragment;
        this.saverStatus = new SaverStatus(fragment.getContext());
        this.gson = new Gson();
    }

    public void handle(String topic, String message) {
        if(topic.equals(MqttManager.TAMAGOTCHI_STATUS_CHECK_SLEEP_TOPIC)) {
            boolean statusSleep = Boolean.valueOf(message);
            fragment.setStatusSleep(statusSleep);
        }

        if(topic.equals(MqttManager.TAMAGOTCHI_PET_STATE_TOPIC) && !message.equals(TamagotchiCommands.GET_STATUS_COMMAND))
            handlePetState(message);
        else if(!message.equals(TamagotchiStatuses.HUNGRY_STATUS) && !message.equals(TamagotchiStatuses.TOILET_STATUS) &&
                !message.equals(TamagotchiStatuses.WANNA_PLAY_STATUS) && !message.equals(TamagotchiStatuses.WANNA_WALK_STATUS) &&
                !message.equals(TamagotchiCommands.GET_STATUS_COMMAND)) {
            TamagotchiStatus status;
            try {
                status = gson.fromJson(message, TamagotchiStatus.class);
                handleStatus(status);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void handleStatistics(String topic, String message) {
        if(topic.equals(MqttManager.TAMAGOTCHI_PET_STATE_TOPIC) && !message.equals(TamagotchiCommands.GET_STATUS_COMMAND)) {
            TamagotchiStatus[] statuses = new Gson().fromJson(message, TamagotchiStatus[].class);

            for (TamagotchiStatus status :
                    statuses) {
                saverStatus.saveState(status);
            }

            fragment.showProgresses(statuses);
        }
    }

    private void handlePetState(String response) {
        TamagotchiStatus[] statuses = new Gson().fromJson(response, TamagotchiStatus[].class);

        for (TamagotchiStatus status :
                statuses) {
            saverStatus.saveState(status);
        }

        int average = calculateAverage(statuses);
        showStatusTamagotchi(average);
    }

    private void handleStatus(TamagotchiStatus status) {
        saverStatus.saveState(status);

        TamagotchiStatus eatStatus       = saverStatus.readStatus(TamagotchiStatuses.HUNGRY_STATUS);
        TamagotchiStatus sleepStatus     = saverStatus.readStatus(TamagotchiStatuses.SLEEP_STATUS);
        TamagotchiStatus toiletStatus    = saverStatus.readStatus(TamagotchiStatuses.TOILET_STATUS);

        TamagotchiStatus[] statuses = new TamagotchiStatus[] { eatStatus, sleepStatus, toiletStatus };
        int average = calculateAverage(statuses);
        showStatusTamagotchi(average);
    }

    private int calculateAverage(TamagotchiStatus[] statuses) {
        int average = 0;
        for (TamagotchiStatus status :
                statuses) {

            if(status == null) {
                average += 100;
                continue;
            }

            average += status.getCurrent();
        }
        return average / statuses.length;
    }

    private void showStatusTamagotchi(int average) {
        if(average <= 10) {
            fragment.setStatusTamagotchi(R.drawable.ic_status_happy);
        } else if (average <= 70) {
            fragment.setStatusTamagotchi(R.drawable.ic_status_normal);
        } else {
            fragment.setStatusTamagotchi(R.drawable.ic_status_bad);
        }
    }
}
