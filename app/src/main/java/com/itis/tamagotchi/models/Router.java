package com.itis.tamagotchi.models;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.itis.tamagotchi.R;

public class Router {

    private FragmentManager fragmentManager;

    public Router(AppCompatActivity activity) {
        fragmentManager = activity.getSupportFragmentManager();
    }

    public void startFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .add(R.id.fragment_container, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    public void showFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }
}
