package com.itis.tamagotchi.models.tamagotchi;

public final class TamagotchiCommands {
    public static final String FEED_COMMAND       = "feed";
    public static final String GET_STATUS_COMMAND = "get_pet_state";
    public static final String TOILET_COMMAND     = "toilet";
    public static final String IS_SLEEP_COMMAND   = " is_sleeping";

    public static final String SLEEP_COMMAND(int time) {
        return String.format("%s %d", "sleep", time);
    }
}
