package com.itis.tamagotchi.models.MqttClient;

import android.app.Application;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttManager {

    private static final String BROKER_URL        = "tcp://192.168.14.254";
    private static final String CLIENT_ID         = "team_1";

    public static final String TAMAGOTCHI_MAIN_TOPIC               = "team1/tamagucci";
    public static final String TAMAGOTCHI_PET_STATE_TOPIC          = "team1/tamagucci/pet_state";
    public static final String TAMAGOTCHI_HUNGRY_STATE_TOPIC       = "team1/tamagucci/hungry_status";
    public static final String TAMAGOTCHI_SLEEP_STATUS_TOPIC       = "team1/tamagucci/sleep_status";
    public static final String TAMAGOTCHI_TOILET_STATUS_TOPIC      = "team1/tamagucci/toilet_status";
    public static final String TAMAGOTCHI_WANNA_PLAY_STATUS_TOPIC  = "team1/tamagucci/wanna_play_status";
    public static final String TAMAGOTCHI_WANNA_WALK_STATUS_TOPIC  = "team1/tamagucci/wanna_walk_status";
    public static final String TAMAGOTCHI_STATUS_CHECK_SLEEP_TOPIC = "team1/tamagucci/check_sleep";

    private MqttAndroidClient mqttClient;
    private MqttConnectOptions options;

    public MqttManager(Application app) {
        mqttClient = new MqttAndroidClient(app, BROKER_URL, CLIENT_ID);

        options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
    }

    public void connectToBroker(IMqttActionListener connectListener) throws MqttException {
        mqttClient.connect(options, null, connectListener);
    }

    public void subscribeToTopic(String topic,
                                 IMqttActionListener subscribeListener) throws MqttException {
        mqttClient.subscribe(topic, 0, null, subscribeListener);
    }

    public void setCallback(MqttCallback callback) {
        mqttClient.setCallback(callback);
    }

    public void sendMessage(String topic, String sendingMessage) throws MqttException {
        if(mqttClient.isConnected()) {
            MqttMessage message = new MqttMessage();
            message.setPayload(sendingMessage.getBytes());
            mqttClient.publish(topic, message);
        }
    }


    public boolean isConnected() {
        return mqttClient.isConnected();
    }

    public void disconnect() {
        try {
            if(mqttClient.isConnected())
                mqttClient.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
