package com.itis.tamagotchi.models.database;

public class DatabaseInfo {
    public static final String DEVICE_COLLECTION = "devices";
    public static final String DEVICE_DOCUMENT   = "listDevices";
}
