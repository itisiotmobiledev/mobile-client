package com.itis.tamagotchi.models.tamagotchi;

public class TamagotchiStatus {
    private String title;
    private int min;
    private int current;
    private int max;

    public String getTitle() {
        return title;
    }

    public int getMin() {
        return min;
    }

    public int getCurrent() {
        return current;
    }

    public int getMax() {
        return max;
    }
}
